<?php

namespace App\Http\Controllers;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\User;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function profile(Request $request)
    {
        if(!empty($request['id']))
        {
            $request->validate([
                'name' => 'required|string',
                'username' => 'required|string',
                'email' => 'required|string|regex:/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i'
            ]);
            $user = User::find($request['id']);
            $old_image = basename($user->image);
            if($request->hasFile('img')){
                $user->image = asset('storage/'.Storage::disk('public')->put('profiles',$request['img']));
                Storage::disk('public')->delete('profiles/'.$old_image);
            };
            $user->save();
            return redirect('/profile');
        }
    }
}
