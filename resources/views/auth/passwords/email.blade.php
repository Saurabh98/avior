<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Avior</title>
        <link href="../../css/app.css" rel="stylesheet">
        <style>
            .app-header .navbar-brand
            {
                width: 78px;
                height: 0px;
            }
        </style>
    </head>
    <body>
        <header class="app-header navbar">
                <a class="navbar-brand" href="/" style="width: 78px !important;height: 0px !important;">Avior</a>   
                <ul class="nav navbar-nav ml-auto">
                    @if (Route::has('login'))
                        <div class="top-right links">
                            @auth
                                <a href="/home" class="navbar-brand">Home</a>
                            @else
                                <a href="/" class="navbar-brand">Login</a>
                            @endauth
                        </div>
                    @endif
                </ul>
        </header><br>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Reset Password') }}</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Send Password Reset Link') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/app.js"></script>
    </body>
</html>
