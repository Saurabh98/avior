<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="csrf-token" content="@csrf()">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

<title>Avior</title>
<link rel="stylesheet" href="css/app.css">
<style>
    .profile-container
    {
        width: 500px;
        height: auto;
        padding: 20px;
        background: #fff;
        box-shadow: 1px 2px lightgray;
    }
    .profile-name
    {
        font-size: 26px;
    }
    .profile-image
    {
        width: 100px;
        border-radius: 50%;
    }
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

@include('partials.header')

<div class="app-body">

    @include('partials.sidebar');

    <main class="main">

        <ol class="breadcrumb">
            <a href="/home" style="text-decoration:none;font-size:18px;">Home&nbsp;</a>
            @if(!empty($profile))<a style="text-decoration:none;font-size:18px;"> / {{Auth::user()->username}} / {{$profile}}</a>@endif
        </ol>

        <div class="container-fluid">

            <div id="ui-view">
                @yield('content')
            </div>

        </div>

    </main>

</div>

@include('partials.footer')

<script src="js/app.js"></script>

<!-- <script>
    $('#ui-view').ajaxLoad();
    $(document).ajaxComplete(function() {
      Pace.restart()
    });
</script> -->
</body>
</html>
