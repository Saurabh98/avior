<div class="sidebar">

</div>

<aside class="aside-menu">

    <ul class="nav nav-tabs" role="tablist">
        <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>
        <a class="dropdown-item" href="/profile">
            {{ __('Profile') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form> 
    </ul>

</aside>